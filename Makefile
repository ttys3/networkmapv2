CC = /home/hacklog/router/rt-n56u/toolchain-mipsel/toolchain-3.4.x/bin/mipsel-linux-uclibc-gcc -mips32r2 -march=mips32r2
CFLAGS = -Os -fomit-frame-pointer -ffunction-sections -fdata-sections -fvisibility=hidden -I/home/hacklog/router/rt-n56u/trunk/stage/include -pthread
LD = /home/hacklog/router/rt-n56u/toolchain-mipsel/toolchain-3.4.x/bin/mipsel-linux-uclibc-ld
LDFLAGS = -Wl,--gc-sections -Os -L/home/hacklog/router/rt-n56u/trunk/stage/lib
SHAREDIR      = $(ROOTDIR)/user/shared

EXEC = networkmap
OBJS = function.o networkmap.o

CFLAGS  += -w -DASUS -DBCMNVRAM -I. -I$(SHAREDIR) -I$(SHAREDIR)/include -I$(STAGEDIR)/include
CFLAGS  += -s -O2 -DNO_PARALLEL
CFLAGS       += -D_BSD_SOURCE -D_GNU_SOURCE

LDFLAGS      += -L$(SHAREDIR) -lshared

CFLAGS	+= -DDEBUG -DDEBUG_MORE -DDEBUG_FUNCTION
CFLAGS += -DNMP_DB
CFLAGS += -DRTCONFIG_BONJOUR -DRTCONFIG_UPNPC

all: $(EXEC)

$(EXEC): $(OBJS)
	$(CC) -o $@ $^ $(LDFLAGS) $(CFLAGS)

install:
	install -D $(EXEC) $(INSTALLDIR)/usr/sbin/$(EXEC)
	$(STRIP) $(INSTALLDIR)/usr/sbin/$(EXEC)

clean: 
	rm -rf *.o $(EXEC) *~
